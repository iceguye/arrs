# CREDITS

This project uses the source codes or designs from the following free
and open source projects. This project strongly appreciates their
contributions and inspirations.

**Arweave-js**

Project Page: <https://github.com/ArweaveTeam/arweave-js>

License:
<https://raw.githubusercontent.com/ArweaveTeam/arweave-js/master/LICENSE.md>

**serde**

Project Page: <https://github.com/serde-rs/serde>

License: <https://raw.githubusercontent.com/serde-rs/serde/master/LICENSE-MIT>

**RustCrypto**

Project Page: <https://github.com/RustCrypto>

License: <https://opensource.org/license/mit/>

**reqwest**

Project Page: <https://github.com/seanmonstar/reqwest>

License: <https://raw.githubusercontent.com/seanmonstar/reqwest/master/LICENSE-MIT>

**indexmap**

Project Page: <https://github.com/indexmap-rs/indexmap>

License: <https://raw.githubusercontent.com/indexmap-rs/indexmap/master/LICENSE-MIT>

**openssl**

Project Page: <https://www.openssl.org/>

License: <https://www.openssl.org/source/license.html>

**primitive_types**

Project Page: <https://github.com/paritytech/parity-common>

License: <https://github.com/paritytech/parity-common/blob/master/LICENSE-APACHE2>